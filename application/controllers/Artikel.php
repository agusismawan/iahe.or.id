<?php

class Artikel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_artikel');
    }

    public function index()
    {
        $data['judul'] = 'Artikel';
        $data['art'] = $this->M_artikel->getArtikel()->result_array();
        $data['archive'] = $this->M_artikel->getAllArtikel()->result_array();

        $this->load->view('templates/v_home_header');
        $this->load->view('templates/v_home_topbar');
        $this->load->view('home/v_artikel', $data);
        $this->load->view('templates/v_home_footer');
    }

    public function detail($id)
    {
        $data['art'] = $this->M_artikel->getArtikelById($id)->row_array();
        $data['recent'] = $this->M_artikel->getArtikel()->result_array();
        $data['archive'] = $this->M_artikel->getAllArtikel()->result_array();

        $this->load->view('templates/v_home_header');
        $this->load->view('templates/v_home_topbar');
        $this->load->view('home/v_artikel_detail', $data);
        $this->load->view('templates/v_home_footer');
    }
}
