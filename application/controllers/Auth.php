<?php

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_user');
        $this->load->helper('file');
    }

    public function index()
    {
        $data['judul'] = 'Login';
        if ($this->session->userdata('email')) {
            redirect('admin/dashboard');
        }

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() ==  false) {
            $this->load->view('templates/v_auth_header', $data);
            $this->load->view('auth/v_login');
            $this->load->view('templates/v_auth_footer');
        } else {
            // validasi sukses
            $this->_login();
        }
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $user = $this->db->get_where('t_user', ['user_email' => $email])->row_array();

        // jika usernya ada
        if ($user) {
            // jika usernya aktif
            if ($user['user_isactive'] == 1) {
                // cek password
                if (password_verify($password, $user['user_password'])) {
                    $data = ['email' => $user['user_email']];
                    $this->session->set_userdata($data);
                    $this->session->set_flashdata('pesan', 'Welcome Back!');
                    redirect('admin/dashboard');
                } else {
                    $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible">
                         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> Ada yang salah!</h5>
                            Password Salah!
                        </div>');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i> Ada yang salah!</h5>
                        User belum / tidak aktif!
                    </div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i> Ada yang salah!</h5>
                        Email tidak terdaftar!
                     </div>');
            redirect('auth');
        }
    }

    public function register()
    {
        $data['judul'] = 'Pilihan Pendaftaran';
        $this->load->view('templates/v_auth_header', $data);
        $this->load->view('auth/v_opsireg');
        $this->load->view('templates/v_auth_footer');
    }

    public function validasi()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[t_user.user_email]', ['is_unique' => '{field} sudah terdaftar!', 'required' => '{field} tidak boleh kosong!', 'valid_email' => 'Format email salah!']);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[6]|matches[password2]', ['min_length' => '{field} terlalu pendek!', 'matches' => 'Password tidak sama!', 'required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
        $this->form_validation->set_rules('txtTlp', 'Nomor Telepon / HP', 'required|trim|is_unique[t_user.user_notlp]', ['required' => '{field} tidak boleh kosong!', 'is_unique' => '{field} sudah terdaftar!']);
        $this->form_validation->set_rules('txtAlamat', 'Alamat', 'required|trim', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtInstansi', 'Instansi', 'required|trim', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtJabatan', 'Nama Jabatan', 'required|trim', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtBidang', 'Bidang Ilmu', 'required|trim', ['required' => '{field} tidak boleh kosong!']);
        // $this->form_validation->set_rules('image', 'Foto', 'callback_cek_files');
        // $this->form_validation->set_rules('cv', 'CV', 'callback_cek_files');
        if (empty($_FILES['image']['name'])) {
            $this->form_validation->set_rules('image', 'Foto', 'required', ['required' => '{field} tidak boleh kosong!']);
        }
        if (empty($_FILES['cv']['name'])) {
            $this->form_validation->set_rules('cv', 'CV', 'required', ['required' => '{field} tidak boleh kosong!']);
        }
    }

    public function korporasi()
    {
        $this->form_validation->set_rules('txtJenisUsaha', 'Jenis Usaha', 'required|trim', ['required' => '{field} tidak boleh kosong!']);
        $this->form_validation->set_rules('txtNama', 'Contact Person', 'required|trim', ['required' => '{field} tidak boleh kosong!']);
        $this->validasi();

        if ($this->form_validation->run() == false) {
            $data['judul'] = 'Pendaftaran Anggota Korporasi';
            $this->load->view('templates/v_auth_header', $data);
            $this->load->view('auth/v_register-korporasi');
            $this->load->view('templates/v_auth_footer');
        } else {
            $nama = $this->input->post('txtNama');
            $email = $this->input->post('email');
            $this->cek_files();
            $this->session->set_flashdata('pesan', ' <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-check"></i> Sukses!</h5>
                 Hai <strong>' . $nama . '</strong>, Pendaftaran Berhasil. Admin akan mereview data anda pastikan data yang anda masukkan benar, selanjutnya anda akan kami hubungi melalui Email (' . $email . ').
            </div>');
            redirect('auth/sukses');
        }
    }

    public function perorangan()
    {
        $this->form_validation->set_rules('txtNama', 'Nama Lengkap', 'required|trim', ['required' => '{field} tidak boleh kosong!']);
        $this->validasi();

        if ($this->form_validation->run() == false) {
            $data['judul'] = 'Pendaftaran Anggota Perorangan';
            $this->load->view('templates/v_auth_header', $data);
            $this->load->view('auth/v_register');
            $this->load->view('templates/v_auth_footer');
        } else {
            $nama = $this->input->post('txtNama');
            $email = $this->input->post('email');
            $this->cek_files();
            $this->session->set_flashdata('pesan', ' <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-check"></i> Sukses!</h5>
                         Hai <strong>' . $nama . '</strong>, Pendaftaran Berhasil. Admin akan mereview data anda pastikan data yang anda masukkan benar, selanjutnya anda akan kami hubungi melalui Email (' . $email . ').
                    </div>');
            redirect('auth/sukses');
        }
    }

    public function sukses()
    {
        $data['judul'] = 'Registrasi Berhasil';
        $this->load->view('templates/v_auth_header', $data);
        $this->load->view('auth/v_reg-sukses');
        $this->load->view('templates/v_auth_footer');
    }

    function cek_files()
    {
        $this->load->library('upload');

        $config['max_size']         = '5120';
        $config['upload_path']      = './assets/img/profile/';
        $config['allowed_types']    = 'jpg|png|jpeg';
        $config['file_name']        = 'img_' . substr(md5(rand()), 0, 7);
        $config['overwrite']        = FALSE;

        $this->upload->initialize($config);
        if (!$this->upload->do_upload('image')) {
            $this->session->set_flashdata('pesan', $this->upload->display_errors());
            return false;
        } else {
            $upload_image = $this->upload->data();

            unset($config);
            $config['max_size']         = '5120';
            $config['upload_path']      = './assets/doc/';
            $config['allowed_types']    = 'pdf';
            $config['file_name']        = 'file_' . substr(md5(rand()), 0, 7);
            $config['overwrite']        = FALSE;

            $this->upload->initialize($config);
            if (!$this->upload->do_upload('cv')) {
                $this->session->set_flashdata('pesan', $this->upload->display_errors());
                return false;
            } else {
                $upload_cv = $this->upload->data();

                //get the uploaded file name
                $data['image'] = $upload_image['file_name'];
                $data['cv'] = $upload_cv['file_name'];
                $data['type'] = $this->uri->segment(2);

                //store files data to the db
                $this->M_user->addNewMember($data);
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('email');
        redirect('auth');
    }
}
