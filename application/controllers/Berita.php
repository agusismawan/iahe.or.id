<?php

class Berita extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_berita');
    }

    public function index()
    {
        $data['brt'] = $this->M_berita->getBerita()->result_array();
        $data['archive'] = $this->M_berita->getAllBerita()->result_array();

        $this->load->view('templates/v_home_header');
        $this->load->view('templates/v_home_topbar');
        $this->load->view('home/v_berita', $data);
        $this->load->view('templates/v_home_footer');
    }

    public function detail($id)
    {
        $data['brt'] = $this->M_berita->getBeritaById($id)->row_array();
        $data['recent'] = $this->M_berita->getBerita()->result_array();
        $data['archive'] = $this->M_berita->getAllBerita()->result_array();

        $this->load->view('templates/v_home_header');
        $this->load->view('templates/v_home_topbar');
        $this->load->view('home/v_berita_detail', $data);
        $this->load->view('templates/v_home_footer');
    }
}
