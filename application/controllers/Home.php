<?php

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_artikel');
        $this->load->model('M_berita');
    }

    public function index()
    {
        $data['art'] = $this->M_artikel->getArtikel()->result_array();
        $data['brt'] = $this->M_berita->getBerita()->result_array();
        // echo "/home/index";
        $this->load->view('templates/v_home_header');
        $this->load->view('templates/v_home_topbar');
        $this->load->view('home/v_home', $data);
        $this->load->view('templates/v_home_footer');
    }

    public function visimisi()
    {
        $this->load->view('templates/v_home_header');
        $this->load->view('templates/v_home_topbar');
        $this->load->view('home/v_visimisi');
        $this->load->view('templates/v_home_footer');
    }

    public function tujuan_fungsi()
    {
        $this->load->view('templates/v_home_header');
        $this->load->view('templates/v_home_topbar');
        $this->load->view('home/v_tujuan_fungsi');
        $this->load->view('templates/v_home_footer');
    }

    public function struktur()
    {
        $this->load->view('templates/v_home_header');
        $this->load->view('templates/v_home_topbar');
        $this->load->view('home/v_struktur');
        $this->load->view('templates/v_home_footer');
    }
}
