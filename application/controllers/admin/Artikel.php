<?php

class Artikel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_artikel');
    }

    public function index()
    {
        $data['user'] = $this->db->get_where('t_user', ['user_email' => $this->session->userdata('email')])->row_array();
        $data['judul'] = 'Artikel';
        $data['artikel'] = $this->M_artikel->getAllArtikel()->result_array();

        $this->load->view('templates/v_header', $data);
        $this->load->view('templates/v_topbar', $data);
        $this->load->view('templates/v_sidebar');
        $this->load->view('admin/v_artikel', $data);
        $this->load->view('templates/v_footer');
    }

    public function add()
    {
        $data['user'] = $this->db->get_where('t_user', ['user_email' => $this->session->userdata('email')])->row_array();
        $data['judul'] = 'Tambah Artikel';

        $this->load->view('templates/v_header', $data);
        $this->load->view('templates/v_topbar', $data);
        $this->load->view('templates/v_sidebar');
        $this->load->view('admin/v_artikel_tambah', $data);
        $this->load->view('templates/v_footer');
    }

    public function do_add()
    {
        $data = $this->db->get_where('t_user', ['user_email' => $this->session->userdata('email')])->row_array();
        $user = $data['user_id'];

        $this->cek_img();
        if (!$this->upload->do_upload('image')) {
            $this->session->set_flashdata('pesan', $this->upload->display_errors());
            // redirect('admin/artikel/add');
            return false;
        } else {
            $art_pic = $this->upload->data('file_name');
            $this->db->set('art_img', $art_pic);
        }

        $this->M_artikel->addNewArtikel($user);
        if ($this->db->affected_rows() == 1) {
            $this->session->set_flashdata('pesan', 'Email penolakan berhasil dikirim');
            redirect('admin/artikel');
        }
    }

    public function edit()
    {
        $data['user'] = $this->db->get_where('t_user', ['user_email' => $this->session->userdata('email')])->row_array();
        $data['judul'] = 'Edit Artikel';
        // $id = $this->uri->segment(4);
        // $data['artikel'] = $this->M_artikel->getArtikelById($id)->row_array();
        $data['artikel'] = $this->db->get_where('t_artikel', ['art_id' => $this->uri->segment(4)])->row_array();

        $this->form_validation->set_rules('txtJudul', 'Judul', 'required');
        $this->form_validation->set_rules('txtKonten', 'Konten', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/v_header', $data);
            $this->load->view('templates/v_topbar', $data);
            $this->load->view('templates/v_sidebar');
            $this->load->view('admin/v_artikel_edit', $data);
            $this->load->view('templates/v_footer');
        } else {
            // cek jika ada gambar yang akan di upload
            $upload_image = $_FILES['image']['name'];

            if ($upload_image) {
                $this->cek_img();
                if ($this->upload->do_upload('image')) {
                    $old_image = $data['artikel']['art_img'];

                    // unlink(FCPATH . 'assets/img/artikel/' . $old_image);

                    $new_image = $this->upload->data('file_name');
                    $this->db->set('art_img', $new_image);
                } else {
                    echo $this->upload->display_errors();
                }
            }
            if ($this->db->affected_rows() == 1) {
                $this->M_artikel->editArtikel();
            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(4);
        $this->M_artikel->deleteArtikel($id);
        if ($this->db->affected_rows() == 1) {
            $this->session->set_flashdata('pesan', 'Artikel berhasil dihapus');
            redirect('admin/artikel');
        }
    }

    public function cek_img()
    {
        $this->load->library('upload');

        $config['max_size']         = '5120';
        $config['upload_path']      = './assets/img/artikel/';
        $config['allowed_types']    = 'jpg|png|jpeg';
        $config['file_name']        = 'img_' . substr(md5(rand()), 0, 7);
        $config['overwrite']        = FALSE;

        $this->upload->initialize($config);
    }
}
