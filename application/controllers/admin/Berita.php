<?php

class Berita extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_berita');
    }

    public function index()
    {
        $data['user'] = $this->db->get_where('t_user', ['user_email' => $this->session->userdata('email')])->row_array();
        $data['judul'] = 'Berita';
        $data['berita'] = $this->M_berita->getAllBerita()->result_array();

        $this->load->view('templates/v_header', $data);
        $this->load->view('templates/v_topbar', $data);
        $this->load->view('templates/v_sidebar');
        $this->load->view('berita/v_index', $data);
        $this->load->view('templates/v_footer');
    }

    public function add()
    {
        $data['user'] = $this->db->get_where('t_user', ['user_email' => $this->session->userdata('email')])->row_array();
        $data['judul'] = 'Tambah Berita';

        $this->load->view('templates/v_header', $data);
        $this->load->view('templates/v_topbar', $data);
        $this->load->view('templates/v_sidebar');
        $this->load->view('berita/v_tambah', $data);
        $this->load->view('templates/v_footer');
    }

    public function do_add()
    {
        $data = $this->db->get_where('t_user', ['user_email' => $this->session->userdata('email')])->row_array();
        $user = $data['user_id'];

        $this->cek_img();
        if (!$this->upload->do_upload('image')) {
            $this->session->set_flashdata('pesan', $this->upload->display_errors());
            redirect('admin/artikel/add');
            return false;
        } else {
            $brt_pic = $this->upload->data('file_name');
            $this->db->set('brt_img', $brt_pic);
        }

        $this->M_berita->addNewBerita($user);
        if ($this->db->affected_rows() == 1) {
            $this->session->set_flashdata('pesan', 'Berita berhasil ditambahkan');
            redirect('admin/berita');
        }
    }

    public function edit()
    {
        $data['user'] = $this->db->get_where('t_user', ['user_email' => $this->session->userdata('email')])->row_array();
        $data['judul'] = 'Edit Berita';
        $data['berita'] = $this->db->get_where('t_berita', ['brt_id' => $this->uri->segment(4)])->row_array();

        $this->form_validation->set_rules('txtJudul', 'Judul', 'required');
        $this->form_validation->set_rules('txtKonten', 'Konten', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/v_header', $data);
            $this->load->view('templates/v_topbar', $data);
            $this->load->view('templates/v_sidebar');
            $this->load->view('berita/v_edit', $data);
            $this->load->view('templates/v_footer');
        } else {
            // cek jika ada gambar yang akan di upload
            $upload_image = $_FILES['image']['name'];

            if ($upload_image) {
                $this->cek_img();
                if ($this->upload->do_upload('image')) {
                    $old_image = $data['berita']['brt_img'];

                    // unlink(FCPATH . 'assets/img/berita/' . $old_image);

                    $new_image = $this->upload->data('file_name');
                    $this->db->set('brt_img', $new_image);
                } else {
                    echo $this->upload->display_errors();
                }
            }
            if ($this->db->affected_rows() == 1) {
                $this->M_berita->editBerita();
            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(4);
        $this->M_berita->deleteBerita($id);
        if ($this->db->affected_rows() == 1) {
            $this->session->set_flashdata('pesan', 'Berita berhasil dihapus');
            redirect('admin/berita');
        }
    }

    public function cek_img()
    {
        $this->load->library('upload');

        $config['max_size']         = '5120';
        $config['upload_path']      = './assets/img/berita/';
        $config['allowed_types']    = 'jpg|png|jpeg';
        $config['file_name']        = 'img_' . substr(md5(rand()), 0, 7);
        $config['overwrite']        = FALSE;

        $this->upload->initialize($config);
    }
}
