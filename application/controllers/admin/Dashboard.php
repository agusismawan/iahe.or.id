<?php

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['user'] = $this->db->get_where('t_user', ['user_email' => $this->session->userdata('email')])->row_array();
        $data['judul'] = 'Dashboard';
        $this->load->view('templates/v_header', $data);
        $this->load->view('templates/v_topbar', $data);
        $this->load->view('templates/v_sidebar');
        $this->load->view('admin/index', $data);
        $this->load->view('templates/v_footer');
    }
}
