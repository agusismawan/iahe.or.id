<?php

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_user');
        $this->load->library('email');
    }

    public function index()
    {
        $data['user'] = $this->db->get_where('t_user', ['user_email' => $this->session->userdata('email')])->row_array();
        $data['judul'] = 'Member';
        $data['member'] = $this->M_user->getAllMember()->result_array();
        $data['calon'] = $this->M_user->getAllNewMember()->result_array();
        $data['type'] = ['perorangan', 'korporasi'];

        $this->load->view('templates/v_header', $data);
        $this->load->view('templates/v_topbar', $data);
        $this->load->view('templates/v_sidebar');
        $this->load->view('user/v_index', $data);
        $this->load->view('templates/v_footer');
    }

    public function edit()
    {
        $this->M_user->editMember();
        if ($this->db->affected_rows() == 1) {
            $this->session->set_flashdata('pesan', 'Data member berhasil diubah');
            redirect('admin/user');
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(4);
        $this->M_user->deleteMember($id);
        if ($this->db->affected_rows() == 1) {
            $this->session->set_flashdata('pesan', 'Data member berhasil dihapus');
            redirect('admin/user');
        }
    }

    public function confirm($id)
    {
        $calon = $this->M_user->getUserById($id);

        $this->_sendEmail();
        $this->email->from('admin@iahe.or.id', 'Administrator IAHE');
        $this->email->to($calon['user_email']);
        $this->email->subject('Konfirmasi Pendaftaran');
        $this->email->message('Selamat ' . $calon['user_nama'] . ', anda diterima sebagai anggota PTPI dan kini akun anda telah aktif.');
        if ($this->email->send()) {
            return true;
        } else {
            echo $this->email->print_debugger();
            $this->session->set_flashdata('error', '<div class="alert alert-danger" role="alert">
							' . $this->email->print_debugger() . '
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    		<span aria-hidden="true">&times;</span>
					  		</button>
						</div>');
            die;
        }
        // $this->session->set_flashdata('pesan', 'Email dikirm, User berhasil dikonfirmasi');
        // redirect('admin/user');
    }

    public function tolak()
    {
        $id = $this->input->post('txtId');

        $this->_sendEmail();
        $this->email->from('admin@iahe.or.id', 'Administrator IAHE');
        $this->email->to($this->input->post('txtEmail'));
        $this->email->subject('Konfirmasi Pendaftaran');
        $this->email->message($this->input->post('txtAlasan'));
        if ($this->email->send()) {
            return true;
            $this->M_user->deleteMember($id);
            $this->session->set_flashdata('pesan', 'Email penolakan berhasil dikirim');
            redirect('admin/user');
        } else {
            echo $this->email->print_debugger();
            $this->session->set_flashdata('error', '<div class="alert alert-danger" role="alert">
							' . $this->email->print_debugger() . '
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    		<span aria-hidden="true">&times;</span>
					  		</button>
						</div>');
            die;
        }
    }

    private function _sendEmail()
    {
        $config = [
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.iahe.or.id',
            'smtp_user' => 'admin@iahe.or.id',
            'smtp_pass' => 'Bukitind4h',
            'smtp_port' => 465,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        ];

        $this->email->initialize($config);
    }
}
