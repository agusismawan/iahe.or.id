<?php

class M_artikel extends CI_Model
{
    public function addNewArtikel($user)
    {
        $data = [
            'art_judul' => $this->input->post('txtJudul', true),
            'art_konten' => $this->input->post('txtKonten', true),
            // 'art_img' => $data['image'],
            'art_datecreated' => date('Y-m-d H:i:s'),
            'art_userlog' => $user
        ];
        $this->db->insert('t_artikel', $data);
    }

    public function getArtikel()
    {
        $this->db->order_by('art_datecreated', 'DESC');
        $this->db->limit(3);
        $this->db->where('art_deleted', 0);
        return $this->db->get('t_artikel');
    }

    public function getAllArtikel()
    {
        $this->db->order_by('art_datecreated', 'DESC');
        $this->db->where('art_deleted', 0);
        return $this->db->get('t_artikel');
    }

    public function getArtikelById($id)
    {
        // return $this->db->get_where('t_artikel', ['art_id' => $id]);
        $this->db->select('*');
        $this->db->from('t_artikel');
        $this->db->join('t_user', 't_user.user_id = t_artikel.art_userlog');
        $this->db->where('t_artikel.art_id', $id);
        $query = $this->db->get();

        return $query;
    }

    public function editArtikel()
    {
        $data = [
            'art_judul' => $this->input->post('txtJudul', true),
            'art_konten' => $this->input->post('txtKonten', true),
            // 'art_img' => $data['image'],
            'art_updatedlog' => date('Y-m-d H:i:s'),
            // 'art_userlog' => $user['user_id'],
        ];

        $this->db->where('art_id', $this->input->post('txtID'));
        $this->db->update('t_artikel', $data);
    }

    public function deleteArtikel($id)
    {
        $this->db->set('art_deleted', 1);
        $this->db->where('art_id', $id);
        $this->db->update('t_artikel');
    }
}
