<?php

class M_berita extends CI_Model
{
    public function addNewBerita($user)
    {
        $data = [
            'brt_judul' => $this->input->post('txtJudul', true),
            'brt_konten' => $this->input->post('txtKonten', true),
            // 'brt_img' => $data['image'],
            'brt_datecreated' => date('Y-m-d H:i:s'),
            'brt_userlog' => $user
        ];
        $this->db->insert('t_berita', $data);
    }

    public function getBerita()
    {
        $this->db->order_by('brt_datecreated', 'DESC');
        $this->db->limit(3);
        $this->db->where('brt_deleted', 0);
        return $this->db->get('t_berita');
    }

    public function getAllBerita()
    {
        $this->db->order_by('brt_datecreated', 'DESC');
        $this->db->where('brt_deleted', 0);
        return $this->db->get('t_berita');
    }

    public function getBeritaById($id)
    {
        // return $this->db->get_where('t_berita', ['brt_id' => $id]);
        $this->db->select('*');
        $this->db->from('t_berita');
        $this->db->join('t_user', 't_user.user_id = t_berita.brt_userlog');
        $this->db->where('t_berita.brt_id', $id);
        $query = $this->db->get();

        return $query;
    }

    public function editBerita()
    {
        $id = $this->uri->segment(4);
        $data = [
            'brt_judul' => $this->input->post('txtJudul', true),
            'brt_konten' => $this->input->post('txtKonten', true),
            // 'brt_img' => $data['image'],
            'brt_updatedlog' => date('Y-m-d H:i:s'),
            // 'brt_userlog' => $user['user_id'],
        ];

        $this->db->where('brt_id', $id);
        $this->db->update('t_berita', $data);
    }

    public function deleteBerita($id)
    {
        $this->db->set('brt_deleted', 1);
        $this->db->where('brt_id', $id);
        $this->db->update('t_berita');
    }
}
