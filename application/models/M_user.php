<?php

class M_user extends CI_Model
{
    public function addNewMember($data)
    {
        $data = [
            'user_jenis_usaha' => htmlspecialchars($this->input->post('txtJenisUsaha', true)),
            'user_nama' => htmlspecialchars($this->input->post('txtNama', true)),
            'user_email' => htmlspecialchars($this->input->post('email', true)),
            'user_password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            'user_notlp' => htmlspecialchars($this->input->post('txtTlp', true)),
            'user_alamat' => htmlspecialchars($this->input->post('txtAlamat', true)),
            'user_jabatan' => htmlspecialchars($this->input->post('txtJabatan', true)),
            'user_bidang_ilmu' => htmlspecialchars($this->input->post('txtBidang', true)),
            'user_instansi' => htmlspecialchars($this->input->post('txtInstansi', true)),
            'user_image' => $data['image'],
            'user_cv' => $data['cv'],
            'user_type' => $data['type'],
            'user_role' => 'member',
            'user_isactive' => 0,
            'user_date_created' => date('Y-m-d H:i:s')
        ];
        $this->db->insert('t_user', $data);
    }

    public function getAllMember()
    {
        $this->db->select('*');
        $this->db->from('t_user');
        $this->db->where('t_user.user_role', 'member');
        $this->db->where('t_user.user_isactive', 1);
        $this->db->where('t_user.user_archived', 0);
        $this->db->order_by('user_date_created', 'DESC');
        $query = $this->db->get();
        return $query;
    }

    public function getAllNewMember()
    {
        $this->db->select('*');
        $this->db->from('t_user');
        $this->db->where('t_user.user_role', 'member');
        $this->db->where('t_user.user_isactive', 0);
        $this->db->where('t_user.user_archived', 0);
        $this->db->order_by('user_date_created', 'DESC');
        $query = $this->db->get();
        return $query;
    }

    public function editMember()
    {
        $data = [
            'user_jenis_usaha' => htmlspecialchars($this->input->post('txtJenisUsaha', true)),
            'user_nama' => htmlspecialchars($this->input->post('txtNama', true)),
            // 'user_password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            'user_notlp' => htmlspecialchars($this->input->post('txtTlp', true)),
            'user_alamat' => htmlspecialchars($this->input->post('txtAlamat', true)),
            'user_jabatan' => htmlspecialchars($this->input->post('txtJabatan', true)),
            'user_bidang_ilmu' => htmlspecialchars($this->input->post('txtBidang', true)),
            'user_instansi' => htmlspecialchars($this->input->post('txtInstansi', true)),
            'user_updated_log' => date('Y-m-d H:i:s')
        ];

        $this->db->where('user_id', $this->input->post('user_id'));
        $this->db->update('t_user', $data);
    }

    public function deleteMember($id)
    {
        $this->db->where('user_id', $id);
        $this->db->delete('t_user');
    }

    public function getUserById($id)
    {
        return $this->db->get_where('t_user', ['user_id' => $id])->row_array();
    }
}
