<div class="login-box">
    <div class="login-logo mb-4">
        <!-- <a href="#"><b>Login</b>User</a> -->
        <img src="<?= base_url('assets/img/'); ?>logo-ptpi.png" alt="" width="75px">
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Masuk untuk memulai sesi</p>

            <?= $this->session->flashdata('pesan'); ?>

            <form action="<?= base_url('auth'); ?>" method="post">
                <div class="input-group mb-0">
                    <input type="text" class="form-control" name="email" placeholder="Email" value="<?= set_value('email'); ?>">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <?= form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>

                <div class="input-group mb-3 mt-3">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>

                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember">
                            <label for="remember">
                                Ingat saya
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <!-- /.social-auth-links -->
            <br>
            <p class="mb-0">
                <a href="<?= base_url('auth/register'); ?>" class="text-center">Pendaftaran anggota baru</a> <br>
                <a href="<?= base_url(); ?>" class="text-center">Kembali ke Home</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
    <p style="text-align:center; font-size:12px;">Copyright &copy; 2019 Persatuan Teknik Perumahsakitan Indonesia</p>
</div>
<!-- /.login-box -->