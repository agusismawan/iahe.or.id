<div class="register-box">
    <div class="register-logo">
        <a href="#"><b>Pendaftaran</b>Anggota</a>
    </div>

    <div class="card">
        <div class="card-body register-card-body">
            <p class="login-box-msg">Pilih Jenis Pendaftaran</p>
            <div class="row mt-3">
                <!-- /.col -->
                <div class="col-5 ml-4">
                    <a href="<?= base_url('auth/perorangan') ?>" class="btn btn-block bg-gradient-primary opsi"><span class="fas fa-user" id="personal"></span> Personal</a>
                </div>
                <div class="col-5">
                    <a href="<?= base_url('auth/korporasi') ?>" type="button" class="btn btn-block bg-gradient-success opsi" id="korporasi"><span class="fas fa-building"></span> Korporasi</a>
                </div>
                <!-- /.col -->
            </div>
            <br>
            <a href="<?= base_url('auth'); ?>"><span class="fas fa-long-arrow-alt-left"></span> Kembali</a>
        </div>
        <!-- /.form-box -->
    </div><!-- /.card -->
    <p style="text-align:center; font-size:12px;">Copyright &copy; 2019 Persatuan Teknik Perumahsakitan Indonesia</p>
</div>
<!-- /.register-box -->