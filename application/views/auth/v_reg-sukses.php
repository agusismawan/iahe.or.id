<!-- <style>
    .tengah {
        margin: auto;
        /* position: absolute; */
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        /* height: 300px; */
        /* border: 1px solid #999; */
        /*jika ingin melihat posisi div anda lebih jelas */
    }
</style> -->

<div class="col-7">
    <div class="login-logo mb-4">
        <!-- <a href="#"><b>Login</b>User</a> -->
        <img src="<?= base_url('assets/img/'); ?>logo-ptpi.png" alt="" width="75px">
    </div>

    <div class="card">
        <div class="card-body">
            <?= $this->session->flashdata('pesan'); ?>
            <a href="<?= base_url(); ?>" class="text-center"><i class="fas fa-home"></i> Kembali ke Home</a>
        </div>
    </div>
    <p style="text-align:center; font-size:12px;">Copyright &copy; 2019 Persatuan Teknik Perumahsakitan Indonesia</p>
</div>