<div class="register-box">
    <div class="register-logo">
        <a href="#"><b>Pendaftaran</b>Perorangan</a>
    </div>

    <div class="card">
        <div class="card-body register-card-body">
            <p class="login-box-msg">Pendaftaran anggota baru</p>
            <div class="alert alert-warning" role="alert">
                <small> <i class="fas fa-exclamation-triangle"></i> Perhatian! Isi terlebih dahulu Formulir <strong>Curriculum Vitae</strong> bisa diunduh (<a href="<?= base_url('assets/doc'); ?>/CV_Calon_Anggota_PTPI.docx">disini</a>), kemudian submit kedalam form pendaftaran dibawah.</small>
            </div>

            <?= $this->session->flashdata('pesan'); ?>
            <?= form_open_multipart('auth/perorangan'); ?>
            <div class="input-group mb-0">
                <input type="text" class="form-control" placeholder="Nama Lengkap" name="txtNama" value="<?= set_value('txtNama'); ?>">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user"></span>
                    </div>
                </div>
            </div>
            <?= form_error('txtNama', '<small class="text-danger pl-3">', '</small>'); ?>

            <div class="input-group mb-0 mt-3">
                <input type="text" class="form-control" placeholder="Email" name="email" value="<?= set_value('email'); ?>">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
            </div>
            <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>

            <div class="input-group mb-0 mt-3">
                <input type="password" class="form-control" placeholder="Password" name="password1">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>

            <div class="input-group mb-0 mt-3">
                <input type="password" name="password2" class="form-control" placeholder="Ulangi password">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>

            <div class="input-group mb-0 mt-3">
                <input type="text" class="form-control" placeholder="Nomor Telepon / HP" name="txtTlp" value="<?= set_value('txtTlp'); ?>" onkeypress="return hanyaAngka(event)" maxlength="13">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-phone"></span>
                    </div>
                </div>
            </div>
            <?= form_error('txtTlp', '<small class="text-danger pl-3">', '</small>'); ?>

            <div class="form-group">
                <textarea name="txtAlamat" class="form-control mt-3" id="" rows="4" placeholder="Tempat tinggal / Domisili"><?= set_value('txtAlamat'); ?></textarea>
                <?= form_error('txtAlamat', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>

            <div class="form-group">
                <input type="text" class="form-control mt-3" placeholder="Nama Instansi" name="txtInstansi" value="<?= set_value('txtInstansi'); ?>">
                <?= form_error('txtInstansi', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>

            <div class="form-group">
                <input type="text" class="form-control mt-3" placeholder="Nama Jabatan" name="txtJabatan" value="<?= set_value('txtJabatan'); ?>">
                <?= form_error('txtJabatan', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>

            <div class="form-group mt-3">
                <input type="text" class="form-control" placeholder="Bidang Ilmu" name="txtBidang" value="<?= set_value('txtBidang'); ?>">
                <small class="font-italic text-muted">*Contoh : Biomedical Engineering</small><br>
                <?= form_error('txtBidang', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>

            <div class="form-group">
                <label for="image">Foto 4x6</label>
                <input type="file" class="form-control-file" id="" name="image">
                <small class="font-italic text-muted mb-0">*File harus berekstensi <strong>.jpg/.jpeg/.png</strong> Maks 2MB</small><br>
                <?= form_error('image', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>

            <div class="form-group">
                <label for="cv">Curriculum Vitae</label>
                <input type="file" class="form-control-file" id="" name="cv">
                <small class="font-italic text-muted">*File harus berekstensi <strong>.pdf</strong> Maks 2MB</small><br>
                <?= form_error('cv', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>

            <div class="row mt-3">
                <div class="col-8">
                    <div class="icheck-primary">
                        <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                        <label for="agreeTerms">
                            Saya setuju dengan <a href="#">ketentuan</a>
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-4">
                    <button type="submit" id="btnRegister" class="btn btn-primary btn-block" disabled="disabled">Daftar</button>
                </div>
                <!-- /.col -->
            </div>
            <?php echo form_close(); ?>
            <br>
            <a href="<?= base_url('auth'); ?>" class="text-center">Saya sudah terdaftar sebagai anggota</a> <br>
            <a href="<?= base_url('auth/register'); ?>" class="text-center">Pilih jenis pendaftaran</a>
        </div>
        <!-- /.form-box -->
    </div><!-- /.card -->
    <p style="text-align:center; font-size:12px;">Copyright &copy; 2019 Persatuan Teknik Perumahsakitan Indonesia</p>
</div>
<!-- /.register-box -->

<script>
    // $("#kategori").change(function() {
    //     console.log($("kategori option:selected").val());
    //     if ($("#kategori option:selected").val() == 'Perorangan') {
    //         $('#perorangan').prop('hidden', false);
    //         $('#korporasi').prop('hidden', 'true');
    //     } else if ($("#kategori option:selected").val() == 'Korporasi') {
    //         $('#korporasi').prop('hidden', false);
    //         $('#perorangan').prop('hidden', 'true');
    //     } else {
    //         $('#perorangan').prop('hidden', 'true');
    //         $('#korporasi').prop('hidden', 'true');
    //     }
    // });

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $("#agreeTerms").click(function() {
        $("#btnRegister").attr("disabled", !this.checked);
    });
</script>