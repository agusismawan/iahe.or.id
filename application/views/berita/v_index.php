<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!-- <h1 class="m-0 text-dark"><?= $judul; ?></h1> -->
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"><?= $judul; ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('pesan'); ?>"></div>
    <?php if ($this->session->flashdata('pesan')) {
        $this->session->flashdata('pesan');
    } else {
        $this->session->flashdata('error');
    } ?>

    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-newspaper"></i>
                            <?= $judul; ?>
                        </h3>
                        <a href="<?= base_url('admin/berita/add'); ?>" class="btn btn-primary btn-xs float-right">
                            <i class="fas fa-plus-circle"></i> Tambah
                        </a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">

                        <table id="example1" class="table table-bordered table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <td>
                                    <?php foreach ($berita as $a) : ?>
                                        <div class="callout callout-primary">
                                            <h5>
                                                <?= $a['brt_judul']; ?>
                                                <a href="<?= base_url('admin/berita/delete/'); ?><?= $a['brt_id']; ?>" onclick="return confirm('Hapus berita <?= $a['brt_judul']; ?> ?');" class="tbl float-right small"><i class="fas fa-trash"></i></a>

                                                <a href="<?= base_url('admin/berita/edit/'); ?><?= $a['brt_id']; ?>" class="tbl float-right small"><i class="fas fa-edit"></i></a>

                                                <small class="tbl float-right">2 menit yang lalu</small>
                                            </h5>
                                            <?php
                                                $string = strip_tags($a['brt_konten']);
                                                if (strlen($string) > 220) {

                                                    // truncate string
                                                    $stringCut = substr($string, 0, 220);
                                                    $endPoint = strrpos($stringCut, ' ');

                                                    //if the string doesn't contain any space then it will cut without word basis.
                                                    $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                                    $string .= '...';
                                                }
                                                echo $string;
                                                ?>
                                        </div>
                                    <?php endforeach; ?>
                                </td>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->