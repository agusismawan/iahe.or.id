<!-- Start Bottom Header -->
<div class="header-bg page-area">
    <div class="home-overly"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="slider-content text-center">
                    <div class="header-bottom">
                        <div class="layer2 wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                            <h1 class="title2"><?= $art['art_judul']; ?> </h1>
                        </div>
                        <div class="layer3 wow zoomInUp" data-wow-duration="2s" data-wow-delay="1s">
                            <!-- <h2 class="title3">profesional Blog Page</h2> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Header -->
<div class="blog-page area-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="page-head-blog">
                    <div class="single-blog-page">
                        <!-- search option start -->
                        <form action="#">
                            <div class="search-option">
                                <input type="text" placeholder="Search...">
                                <button class="button" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </form>
                        <!-- search option end -->
                    </div>
                    <div class="single-blog-page">

                        <!-- recent start -->
                        <div class="left-blog">
                            <h4>recent post</h4>
                            <div class="recent-post">
                                <?php foreach ($recent as $r) : ?>
                                    <div class="recent-single-post">
                                        <div class="post-img">
                                            <a href="<?= base_url('artikel/detail/') . $r['art_id']; ?>">
                                                <img src="<?= base_url('assets/img/artikel/') . $r['art_img'] ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="pst-content">
                                            <p><a href="<?= base_url('artikel/detail/') . $r['art_id']; ?>"><?= $r['art_judul']; ?></a></p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>

                            </div>
                        </div>
                        <!-- recent end -->
                    </div>

                    <div class="single-blog-page">
                        <div class="left-blog">
                            <h4>archive</h4>
                            <ul>
                                <?php foreach ($archive as $arc) : ?>
                                    <li>
                                        <a href="#"><?= date("d F Y", strtotime($arc['art_datecreated'])); ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End left sidebar -->

            <!-- Start single blog -->
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <!-- single-blog start -->
                        <article class="blog-post-wrapper">
                            <div class="post-thumbnail">
                                <img src="<?= base_url('assets/img/artikel/') . $art['art_img'] ?>" alt="" />
                            </div>
                            <div class="post-information">
                                <h2><?= $art['art_judul']; ?></h2>
                                <div class="entry-meta">
                                    <span class="author-meta"><i class="fa fa-user"></i> <a href="#"><?= $art['user_nama']; ?></a></span>
                                    <span><i class="fa fa-clock-o"></i> <?= date("d F Y / H:i:s", strtotime($arc['art_datecreated'])); ?></span>
                                </div>
                                <div class="entry-content">
                                    <?= $art['art_konten']; ?>
                                </div>
                            </div>
                        </article>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Blog Area -->
<div class="clearfix"></div>