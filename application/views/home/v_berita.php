<div class="header-bg page-area">
    <div class="home-overly"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="slider-content text-center">
                    <div class="header-bottom">
                        <div class="layer2 wow zoomIn" data-wow-duration="1s" data-wow-delay=".4s">
                            <h1 class="title2">Berita</h1>
                        </div>
                        <div class="layer3 wow zoomInUp" data-wow-duration="2s" data-wow-delay="1s">
                            <!-- <h2 class="title3">Profesional Blog Page</h2> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Header -->

<div class="blog-page area-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="page-head-blog">
                    <div class="single-blog-page">
                        <!-- search option start -->
                        <form action="#">
                            <div class="search-option">
                                <input type="text" placeholder="Search...">
                                <button class="button" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </form>
                        <!-- search option end -->
                    </div>
                    <div class="single-blog-page">
                        <!-- recent start -->
                        <div class="left-blog">
                            <h4>recent post</h4>
                            <div class="recent-post">
                                <?php foreach ($brt as $b) : ?>
                                    <!-- start single post -->
                                    <div class="recent-single-post">
                                        <div class="post-img">
                                            <a href="<?= base_url('berita/detail/') . $b['brt_id']; ?>">
                                                <img src="<?= base_url('assets/img/berita/') . $b['brt_img'] ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="pst-content">
                                            <p><a href="<?= base_url('berita/detail/') . $b['brt_id']; ?>"> <?= $b['brt_judul']; ?></a></p>
                                        </div>
                                    </div>
                                    <!-- End single post -->
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <!-- recent end -->
                    </div>

                    <div class="single-blog-page">
                        <div class="left-blog">
                            <h4>archive</h4>
                            <ul>
                                <?php foreach ($archive as $arc) : ?>
                                    <li>
                                        <a href="#"><?= date("d F Y", strtotime($arc['brt_datecreated'])); ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End left sidebar -->

            <!-- Start single blog -->
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php foreach ($brt as $bb) : ?>
                            <div class="single-blog">
                                <div class="single-blog-img">
                                    <a href="<?= base_url('berita/detail/') . $bb['brt_id']; ?>">
                                        <img src="<?= base_url('assets/img/berita/') . $bb['brt_img'] ?>" alt="">
                                    </a>
                                </div>
                                <div class="blog-meta">
                                    <span class="date-type">
                                        <i class="fa fa-calendar"></i><?= date("d F Y / H:i:s", strtotime($bb['brt_datecreated'])); ?>
                                    </span>
                                </div>
                                <div class="blog-text">
                                    <h4>
                                        <a href="<?= base_url('berita/detail/') . $bb['brt_id']; ?>"><?= $bb['brt_judul'] ?></a>
                                    </h4>
                                    <p>
                                        <?php
                                            $string = strip_tags($bb['brt_konten']);
                                            if (strlen($string) > 150) {

                                                // truncate string
                                                $stringCut = substr($string, 0, 150);
                                                $endPoint = strrpos($stringCut, ' ');

                                                //if the string doesn't contain any space then it will cut without word basis.
                                                $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                                $string .= '...';
                                            }
                                            echo $string;
                                            ?>
                                    </p>
                                </div>
                                <span>
                                    <a href="<?= base_url('berita/detail/') . $bb['brt_id']; ?>" class="ready-btn">Read more</a>
                                </span>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <!-- End single blog -->
                    <div class="blog-pagination">
                        <ul class="pagination">
                            <li><a href="#">&lt;</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">&gt;</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Blog Area -->

<div class="clearfix"></div>