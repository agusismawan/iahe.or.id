<!-- Start About area -->
<div id="about" class="service-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline text-center">
                    <br><br>
                    <h2>Struktur Organisasi</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <img src="<?= base_url('assets/img/'); ?>struktur.png" alt="" width="700px" style="display: block; margin: 0 auto;">
            <br><br>
            <!-- single-well start-->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="well-left">
                    <div class="single-well">
                        <h4 class="sec-head"><i class="fa fa-user"></i> WAPRES BIDANG INDUSTRI</h4>
                        <p>
                            Industri: seluruh stakeholder berkaitan dengan teknik perumahsakitan (perusahaan alkes, perusahaan pemeliharaan alkes, perusahaan konstruksi rumah sakit, dll)
                        </p>
                        <strong>Tugas :</strong>
                        <ul>
                            <li>
                                <i class="fa fa-check"></i> Mengumpulkan data dan memetaan Industri berkaitan dengan rumah sakit
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Merekrut Industri untuk menjadi anggota ptpi
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Meredefinisi dan mereposisi fungsi Industri untuk mewujudkan rumah sakit yang smart
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mereview seluruh regulasi dan aturan yang berkaitan dengan pengelolaan rumah sakit
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Membuat rekomendasi regulasi dan aturan pengelolaan organisasi dalam rangka mewujudkan RS yang smart
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola training berkaitan dengan manajemen rumah sakit smart
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola audit dan sertifikasi manajemen rumah sakit smart
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola kegiatan / event nasional atau internasional berkaitan dengan manajemen rumah sakit
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola kerjasama antara organisasi dalam dan luar negeri
                            </li>
                        </ul>
                        <br>
                        <!-- *********************************** -->
                        <h4 class="sec-head"><i class="fa fa-user"></i> WAPRES BIDANG PENGELOLAAN SDM</h4>
                        <p>
                            SDM: seluruh ahli teknik berkaitan dengan perumahsakitan (elektro, mesin, sipil, lingkungan, komputer, kimia)
                        </p>
                        <strong>Tugas :</strong>
                        <ul>
                            <li>
                                <i class="fa fa-check"></i> Mengumpulkan data (data base) dan memetakan sdm berkaitan dengan teknik perumahsakitan
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Merekrut anggota perorangan ptpi
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Meredefinisi dan mereposisi fungsi sdm untuk mewujudkan rumah sakit yang smart
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mereview seluruh regulasi dan aturan yang berkaitan dengan pengelolaan sdm teknik perumahsakitan
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Membuat rekomendasi regulasi dan aturan pengelolaan sdm dalam rangka mewujudkan RS yang smart
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola training berkaitan dengan peningkatan kompetensi sdm perumahsakitan
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola audit dan sertifikasi sdm ahli teknik rumah sakit smart
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola kegiatan / event nasional atau internasional berkaitan dengan pendidikan dan penelitian teknik perumahsakitan
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola kegiatan konsultasi berkaitan dengan pengelolaan sdm ahli teknik perumahsakitan
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola kerjasama antara ahli teknik dalam dan luar negeri
                            </li>
                        </ul>
                        <br>
                    </div>
                </div>
            </div>
            <!-- single-well end-->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="well-middle">
                    <div class="single-well">
                        <!-- *********************************** -->
                        <h4 class="sec-head"><i class="fa fa-user"></i> WAPRES BIDANG PENGELOLAAN FASILITAS/ASET RUMAH SAKIT</h4>
                        <p>
                            Fasilitas/Aset: seluruh aset / harta berkaitan dengan perumahsakitan (tanah, bangunan, perabot, peralatan, utilitas, instalasi, sistem informasi rumah sakit)
                        </p>
                        <strong>Tugas :</strong>
                        <ul>
                            <li>
                                <i class="fa fa-check"></i> Mengumpulkan data dan memetakan aset/fasilitas perumahsakitan
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mereview seluruh regulasi dan aturan yang berkaitan dengan pengelolaan aset/fasilitas RS
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola training berkaitan dengan pengelolaan aset/fasilitas rumah sakit
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola audit dan sertifikasi aset/fasilitas rumah sakit
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola kegiatan / event nasional atau internasional berkaitan dengan aset rumah sakit
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola kegiatan konsultasi berkaitan dengan pengelolaan sdm ahli teknik perumahsakitan
                            </li>
                        </ul>
                        <br>
                        <!-- *********************************** -->
                        <h4 class="sec-head"><i class="fa fa-user"></i> SEKRETARIS JENDERAL</h4>
                        <br>
                        <strong>Tugas :</strong>
                        <ul>
                            <li>
                                <i class="fa fa-check"></i> Pusat informasi dan hubungan masyarakat
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola administrasi, data dan dokumen organisasi
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola kantor kesekretariatan
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola keuangan dan perpajakan organisasi
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola sistem informasi dan website organisasi
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola kerjasama antara organisasi
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola pertemuan dan seluruh kegiatan organisasi
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola legalitas organisasi

                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola sdm / pegawai tetap organisasi
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola aset organisasi
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola tugas-tugas lainnya yang diberikan oleh presiden
                            </li>
                        </ul>
                        <br>
                        <!-- *********************************** -->
                        <h4 class="sec-head"><i class="fa fa-user"></i> KETUA AHLI TEKNIK</h4>
                        <br>
                        <strong>Tugas :</strong>
                        <ul>
                            <li>
                                <i class="fa fa-check"></i> Mengelola content /kurikulum /silabus bidang teknik yang berkaitan
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengelola content sertifikasi bidang teknik yang berkaitan
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Menjadi ahli teknik sesuai bidangnya untuk melaksanakan pelatihan, penelitian, audit, pengawasan, konsultasi
                            </li>
                            <li>
                                <i class="fa fa-check"></i> MMengelola publikasi keahlian/keilmuan
                            </li>
                        </ul>
                        <br>

                        <!-- <h4 class="sec-head"><i class="fa fa-user"></i> KRITERIA KETUA / ANGGOTA DEWAN PENGAWAS</h4> 
                        <ul>
                            <li>
                                <i class="fa fa-check"></i> Memikili pengalaman sebagai anggota dewan pengawas rumah sakit atau anggota dewan pengawas di lembaga pemerintahan/pendidikan/penelitian
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Memiliki pengalaman / jabatan sekurang-kurangnya direktur di kementerian atau dirut di rumah sakit atau dirut di perusahaan
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Memiliki kompetensi sekurang-kurangnya ahli teknik kepala perumahsakitan atau kompetensi lainnya yang setara
                            </li>
                        </ul>
                        <br>
                        
                        <h4 class="sec-head"><i class="fa fa-user"></i> KRITERIA KETUA / ANGGOTA DEWAN ETIK</h4>
                        <ul>
                            <li>
                                <i class="fa fa-check"></i> Memikili pengalaman sebagai anggota dewan etik rumah sakit atau anggota dewan etik di lembaga pemerintahan/pendidikan/penelitian
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Memiliki kompetensi sekurang-kurangnya ahli teknik kepala perumahsakitan atau kompetensi lainnya yang setara
                            </li>
                        </ul>
                        <br>
                        
                        <h4 class="sec-head"><i class="fa fa-user"></i> KRITERIA PRESIDEN</h4>
                        <ul>
                            <li>
                                <i class="fa fa-check"></i> Memiliki pengalaman sebagai direktur lembaga pemerintahan/pendidikan/penelitian atau direktur utama perusahaan
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Memiliki kompetensi sekurang-kurangnya ahli teknik kepala perumahsakitan atau kompetensi lainnya yang setara
                            </li>
                        </ul>
                        <br>
                        
                        <h4 class="sec-head"><i class="fa fa-user"></i> KRITERIA WAKIL PRESIDEN</h4>
                        <ul>
                            <li>
                                <i class="fa fa-check"></i> Memiliki pengalaman sebagai wakil/deputi direktur lembaga pemerintahan/pendidikan/penelitian atau direktur perusahaan
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Memiliki kompetensi sekurang-kurangnya ahli teknik utama perumahsakitan atau kompetensi lainnya yang setara
                            </li>
                        </ul>
                        <br>
                        
                        <h4 class="sec-head"><i class="fa fa-user"></i> KRITERIA KETUA AHLI TEKNIK</h4>
                        <ul>
                            <li>
                                <i class="fa fa-check"></i> Memiliki pengalaman sebagai wakil/deputi direktur lembaga pemerintahan/pendidikan/penelitian atau direktur perusahaan

                            </li>
                            <li>
                                <i class="fa fa-check"></i> Memiliki kompetensi sekurang-kurangnya ahli teknik utama perumahsakitan atau kompetensi lainnya yang setara
                            </li>
                        </ul>
                        <br> -->
                    </div>
                </div>
            </div>
            <!-- End col-->
        </div>
    </div>
</div>