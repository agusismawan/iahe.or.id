<!-- Start About area -->
<div id="about" class="service-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline text-center">
                    <br><br>
                    <h2>Tujuan dan Fungsi</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- single-well start-->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="well-left">
                    <div class="single-well">
                        <img src="<?= base_url('assets/img/'); ?>goal.jpg" alt="">
                    </div>
                </div>
            </div>
            <!-- single-well end-->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="well-middle">
                    <div class="single-well">
                        <a href="#">
                            <h4 class="sec-head">Tujuan</h4>
                        </a>
                        <p>
                            Menjadi wadah para ahli teknik dan institusi perumahsakitan untuk mewujudkan rumah sakit di indonesia yang selamat, bermutu, aman, berekabaru, dan terjangkau (SMART)
                        </p>

                        <a href="#">
                            <h4 class="sec-head">Fungsi</h4>
                        </a>
                        <ul>
                            <li>
                                <i class="fa fa-check"></i> Menginisiasi dan mendorong terciptanya kebijakan, sumber daya manusia, manajemen sistem/organisasi, asset, dan dukungan finansial untuk mewujudkan rumah sakit di indonesia yang selamat, bermutu, aman, berekabaru, dan terjangkau (SMART)
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Mengawal implementasi kebijakan, sumber daya manusia, manajemen sistem/organisasi, asset, dan dukungan finansial untuk mewujudkan rumah sakit di Indonesia yang SMART
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Memperbaiki terus menerus kebijakan, sumber daya manusia, manajemen sistem/organisasi, asset, dan dukungan finansial untuk mewujudkan rumah sakit SMART
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End col-->
        </div>
    </div>
</div>