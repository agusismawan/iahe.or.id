<!-- Start About area -->
<div id="about" class="service-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline text-center">
                    <br><br>
                    <h2>Visi dan Misi</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- single-well start-->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="well-left">
                    <div class="single-well">
                        <img src="<?= base_url('assets/img/'); ?>visi-misi.jpg" alt="">
                    </div>
                </div>
            </div>
            <!-- single-well end-->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="well-middle">
                    <div class="single-well">
                        <a href="#">
                            <h4 class="sec-head">Visi</h4>
                        </a>
                        <p>
                            Diakui sebagai organisasi utama yang mendukung implementasi rumah sakit yang SMART di Indonesia.
                        </p>

                        <a href="#">
                            <h4 class="sec-head">Misi</h4>
                        </a>
                        <ul>
                            <li>
                                <i class="fa fa-check"></i> Mendorong terciptanya, mengawal implementasi dan memperbaiki terus menerus
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Kebijakan, SDM (engineer and manager), aset, organisasi (sistem manajemen), dan penganggaran (dukungan keuangan)
                            </li>
                            <li>
                                <i class="fa fa-check"></i> Untuk mewujudkan rumah sakit yang smart di indonesia
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End col-->
        </div>
    </div>
</div>