 <!-- Start Footer bottom Area -->
 <footer>
     <div class="footer-area-bottom">
         <div class="container">
             <div class="row">
                 <div class="col-md-6 col-sm-4 col-xs-12">
                     <div class="footer-content">
                         <div class="footer-head">
                             <div class="footer-logo">
                                 <h2><img src="<?= base_url('assets/img/'); ?>logo-ptpi.png" alt="" width="33px"> IAHE</h2>
                             </div>
                             <p><i>Indonesian Association of Hospital Engineering</i><br>
                                 Wadah para ahli teknik dan institusi perumahsakitan untuk mewujudkan rumah sakit di indonesia yang selamat, bermutu, aman, berekabaru, dan terjangkau (SMART)</p>
                             <div class="footer-icons">
                                 <ul>
                                     <li>
                                         <a href="#"><i class="fa fa-facebook"></i></a>
                                     </li>
                                     <li>
                                         <a href="#"><i class="fa fa-twitter"></i></a>
                                     </li>
                                     <li>
                                         <a href="#"><i class="fa fa-google"></i></a>
                                     </li>
                                 </ul>
                             </div>
                         </div>
                     </div>
                 </div>
                 <!-- end single footer -->
                 <div class="col-md-6 col-sm-4 col-xs-12">
                     <div class="footer-content">
                         <div class="footer-head">
                             <h4>informasi</h4>
                             <p>
                                 Jl. Kemang I No.11, RT.10/RW.1, Bangka, Kec. Mampang Prpt., Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12730
                             </p>
                             <div class="footer-contacts">
                                 <p><span>Tel:</span> +6285 641 345 581</p>
                                 <p><span>Email:</span> <a href="mailto:admin@iahe.or.id">admin@iahe.or.id</a></p>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class="footer-area">
         <div class="container">
             <div class="row">
                 <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="copyright text-center">
                         <p>
                             Copyright &copy; <?= date('Y'); ?> <strong>IAHE</strong>. All Rights Reserved
                         </p>
                     </div>
                     <!-- <div class="credits"> -->
                     <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=eBusiness
            -->
                     <!-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
                 </div>
             </div>
         </div>
     </div>
     </div>
 </footer>

 <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

 <!-- JavaScript Libraries -->
 <script src="<?= base_url('assets/blog/'); ?>lib/jquery/jquery.min.js"></script>
 <script src="<?= base_url('assets/blog/'); ?>lib/bootstrap/js/bootstrap.min.js"></script>
 <script src="<?= base_url('assets/blog/'); ?>lib/owlcarousel/owl.carousel.min.js"></script>
 <script src="<?= base_url('assets/blog/'); ?>lib/venobox/venobox.min.js"></script>
 <script src="<?= base_url('assets/blog/'); ?>lib/knob/jquery.knob.js"></script>
 <script src="<?= base_url('assets/blog/'); ?>lib/wow/wow.min.js"></script>
 <script src="<?= base_url('assets/blog/'); ?>lib/parallax/parallax.js"></script>
 <script src="<?= base_url('assets/blog/'); ?>lib/easing/easing.min.js"></script>
 <script src="<?= base_url('assets/blog/'); ?>lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
 <script src="<?= base_url('assets/blog/'); ?>lib/appear/jquery.appear.js"></script>
 <script src="<?= base_url('assets/blog/'); ?>lib/isotope/isotope.pkgd.min.js"></script>

 <!-- Contact Form JavaScript File -->
 <script src="<?= base_url('assets/blog/'); ?>contactform/contactform.js"></script>

 <script src="<?= base_url('assets/blog/'); ?>js/main.js"></script>
 </body>

 </html>