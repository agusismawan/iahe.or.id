<!doctype html>
<html lang="en">

<head>
  <link rel="icon" href="<?= base_url('assets/img/') ?>logo-ptpi.png" />
  <meta charset="utf-8">
  <title>IAHE - Indonesian Association of Hospital Engineering</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <!-- <link href="<?= base_url('assets/blog/'); ?>img/favicon.png" rel="icon"> -->
  <!-- <link href="<?= base_url('assets/blog/'); ?>img/apple-touch-icon.png" rel="apple-touch-icon"> -->

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?= base_url('assets/blog/'); ?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?= base_url('assets/blog/'); ?>lib/nivo-slider/css/nivo-slider.css" rel="stylesheet">
  <link href="<?= base_url('assets/blog/'); ?>lib/owlcarousel/owl.carousel.css" rel="stylesheet">
  <link href="<?= base_url('assets/blog/'); ?>lib/owlcarousel/owl.transitions.css" rel="stylesheet">
  <link href="<?= base_url('assets/blog/'); ?>lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url('assets/blog/'); ?>lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?= base_url('assets/blog/'); ?>lib/venobox/venobox.css" rel="stylesheet">

  <!-- Nivo Slider Theme -->
  <link href="<?= base_url('assets/blog/'); ?>css/nivo-slider-theme.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?= base_url('assets/blog/'); ?>css/style.css" rel="stylesheet">

  <!-- Responsive Stylesheet File -->
  <link href="<?= base_url('assets/blog/'); ?>css/responsive.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: eBusiness
    Theme URL: https://bootstrapmade.com/ebusiness-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body data-spy="scroll" data-target="#navbar-example">
  <div id="preloader"></div>