<header>
  <!-- header-area start -->
  <div id="sticker" class="header-area">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12">

          <!-- Navigation -->
          <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <!-- Brand -->
              <a class="navbar-brand page-scroll sticky-logo" href="<?= base_url(); ?>">
                <h1><img src="<?= base_url('assets/img/'); ?>topbar5.png" alt="" width="350px"></h1>
                <!-- Uncomment below if you prefer to use an image logo -->
              </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Tentang Kami<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="<?= base_url('home/visimisi'); ?>">Visi & Misi</a></li>
                    <li><a href="<?= base_url('home/tujuan_fungsi') ?>">Tujuan dan Fungsi</a></li>
                    <li><a href="<?= base_url('home/struktur') ?>">Struktur Organisasi</a></li>
                  </ul>
                </li>
                <li>
                  <a class="page-scroll" href="<?= base_url(); ?>berita">Berita</a>
                </li>
                <li>
                  <a class="page-scroll" href="<?= base_url(); ?>artikel">Artikel</a>
                </li>
                <li>
                  <a class="page-scroll" href="<?= base_url(); ?>#aktifitas">Aktifitas</a>
                </li>
                <li>
                  <a class="page-scroll" href="<?= base_url(); ?>#contact">Hubungi Kami</a>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Unduh<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="<?= base_url('assets/doc/') ?>AD_PTPI.pdf">Anggaran Dasar (AD)</a></li>
                    <li><a href="<?= base_url('assets/doc/') ?>ART_PTPI.pdf">Anggaran Rumah Tangga (ART)</a></li>
                    <li><a href="<?= base_url('assets/doc/') ?>Akta_Pendirian_PTPI.pdf">Akta Pendirian</a></li>
                  </ul>
                </li>
                <li>
                  <a class="page-scroll" href="<?= base_url('auth'); ?>"><i class="fa fa-user"></i> Login</a>
                </li>
              </ul>
            </div>
            <!-- navbar-collapse -->
          </nav>
          <!-- END: Navigation -->
        </div>
      </div>
    </div>
  </div>
  <!-- header-area end -->
</header>
<!-- header end -->