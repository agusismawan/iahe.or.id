<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><?= $judul; ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"><?= $judul; ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('pesan'); ?>"></div>
    <?php
    if ($this->session->flashdata('pesan')) {
        $this->session->flashdata('pesan');
    } else {
        $this->session->flashdata('error');
    } ?>

    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-success card-outline">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fas fa-hourglass-start"></i> Belum Dikonfirmasi</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Aksi</th>
                                    <th>Foto</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Tipe</th>
                                    <th>Institusi</th>
                                    <th>Jns Usaha</th>
                                    <th>Jabatan</th>
                                    <th>Bid Ilmu</th>
                                    <th>Domisili</th>
                                    <th>Telp</th>
                                    <th>CV</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($calon as $c) : ?>
                                    <tr>
                                        <td><?= $i++; ?></td>
                                        <td>
                                            <a href="<?= base_url(); ?>admin/user/confirm/<?= $c['user_id']; ?>" class="btn btn-primary btn-xs" id="btnAcc"><i class="fas fa-check"></i></a>

                                            <!-- <a href="<?= base_url(); ?>admin/user/delete/<?= $c['user_id']; ?>" class="btn btn-danger btn-xs"><i class="fas fa-times"></i></a> -->

                                            <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-decline<?= $c['user_id']; ?>">
                                                <i class="fas fa-times"></i>
                                            </button>
                                        </td>
                                        <td>
                                            <a href="<?= base_url('assets/img/profile/') . $c['user_image']; ?>" target="_blank">
                                                <img src="<?= base_url('assets/img/profile/') . $c['user_image']; ?>" class="img-circle" alt="<?= $c['user_image']; ?>" width="45px">
                                            </a>
                                        </td>
                                        <td><?= $c['user_nama']; ?></td>
                                        <td><?= $c['user_email']; ?></td>
                                        <td><?= $c['user_type']; ?></td>
                                        <td><?= $c['user_instansi']; ?></td>
                                        <td><?= $c['user_jenis_usaha']; ?></td>
                                        <td><?= $c['user_jabatan']; ?></td>
                                        <td><?= $c['user_bidang_ilmu']; ?></td>
                                        <td><?= $c['user_alamat']; ?></td>
                                        <td><?= $c['user_notlp']; ?></td>
                                        <td>
                                            <a href="<?= base_url('assets/doc/') . $c['user_cv']; ?>"><i class="fas fa-file-pdf"></i> <?= $c['user_cv']; ?></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fas fa-table"></i> Terdaftar</h3>

                        <button type="button" class="btn btn-primary btn-xs float-right" data-toggle="modal" data-target="#modal-tambah">
                            <i class="fas fa-plus-circle"></i> Tambah
                        </button>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Aksi</th>
                                    <th>Foto</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Tipe</th>
                                    <th>Institusi</th>
                                    <th>Jns Usaha</th>
                                    <th>Jabatan</th>
                                    <th>Bid Ilmu</th>
                                    <th>Domisili</th>
                                    <th>Telp</th>
                                    <th>CV</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($member as $m) : ?>
                                    <tr>
                                        <td><?= $i++; ?></td>
                                        <td>
                                            <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-edit<?= $m['user_id']; ?>">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <a href="<?= base_url(); ?>admin/user/delete/<?= $m['user_id']; ?>" class="btn btn-danger btn-xs" onclick="return confirm('Hapus data <?= $m['user_nama']; ?> ?');"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                        <td>
                                            <a href="<?= base_url('assets/img/profile/') . $m['user_image']; ?>" target="_blank">
                                                <img src="<?= base_url('assets/img/profile/') . $m['user_image']; ?>" class="img-circle" alt="<?= $m['user_image']; ?>" width="45px">
                                            </a>
                                        </td>
                                        <td><?= $m['user_nama']; ?></td>
                                        <td><?= $m['user_email']; ?></td>
                                        <td><?= $m['user_type']; ?></td>
                                        <td><?= $m['user_instansi']; ?></td>
                                        <td><?= $m['user_jenis_usaha']; ?></td>
                                        <td><?= $m['user_jabatan']; ?></td>
                                        <td><?= $m['user_bidang_ilmu']; ?></td>
                                        <td><?= $m['user_alamat']; ?></td>
                                        <td><?= $m['user_notlp']; ?></td>
                                        <td>
                                            <a href="<?= base_url('assets/doc/') . $m['user_cv']; ?>"><i class="fas fa-file-pdf"></i> <?= $m['user_cv']; ?></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-tambah">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php foreach ($member as $key) : ?>
    <div class="modal fade" id="modal-edit<?= $key['user_id']; ?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Data Member</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('admin/user/edit'); ?>" method="post">
                    <div class="modal-body">
                        <input type="hidden" value="<?= $m['user_id']; ?>" name="user_id">
                        <div class="form-group">
                            <label for="txtEmail"><strong>Email</strong></label>
                            <input type="text" class="form-control" name="txtEmail" value="<?= $key['user_email']; ?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="txtNama"><strong>Nama Lengkap</strong></label>
                            <input type="text" class="form-control" name="txtNama" value="<?= $key['user_nama']; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="txtTlp"><strong>Telepon / HP</strong></label>
                            <input type="text" class="form-control" name="txtTlp" value="<?= $key['user_notlp']; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="txtAlamat"><strong>Alamat</strong></label>
                            <input type="text" class="form-control" name="txtAlamat" value="<?= $key['user_alamat']; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="txtJabatan"><strong>Jabatan</strong></label>
                            <input type="text" class="form-control" name="txtJabatan" value="<?= $key['user_jabatan']; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="txtBidang"><strong>Bidang Ilmu</strong></label>
                            <input type="text" class="form-control" name="txtBidang" value="<?= $key['user_bidang_ilmu']; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="txtInstansi"><strong>Instansi</strong></label>
                            <input type="text" class="form-control" name="txtInstansi" value="<?= $key['user_instansi']; ?>" required>
                        </div>
                        <?php if ($key['user_jenis_usaha'] != null) : ?>
                            <div class="form-group">
                                <label for="txtJenisUsaha"><strong>Jenis Usaha</strong></label>
                                <input type="text" class="form-control" name="txtJenisUsaha" value="<?= $key['user_jenis_usaha']; ?>" required>
                            </div>
                        <?php else : ?>
                        <?php endif; ?>
                        <div class="form-group">
                            <label for="type"><strong>Jenis Member</strong></label>
                            <select name="type" class="form-control select2" required>
                                <option value="">- Pilih -</option>
                                <?php foreach ($type as $t) : ?>
                                    <option value="<?= $t; ?>" <?= $t == $key['user_type'] ? 'selected' : null ?>><?= $t; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<?php foreach ($calon as $dec) : ?>
    <div class="modal fade" id="modal-decline<?= $dec['user_id']; ?>">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tolak <?= $dec['user_nama']; ?> ?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('admin/user/tolak'); ?>" method="POST">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="txtAlasan"><strong>Berikan alasanmu <span class="text-danger">*</span></strong></label>
                            <input type="hidden" name="txtEmail" id="" value="<?= $dec['user_email']; ?>">
                            <input type="text" name="txtId" id="" value="<?= $dec['user_id']; ?>">
                            <textarea class="form-control" name="txtAlasan" rows="7" required>Silahkan melakukan pendaftaran ulang</textarea>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>


<!-- <script>
    $(document).ready(function() {
        $("#btnDecline").click(function() {
            $("#btnAcc").toggle("slow");
        })
    });
</script> -->