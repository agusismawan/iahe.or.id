-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 11, 2019 at 05:37 
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ptpi`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_artikel`
--

CREATE TABLE IF NOT EXISTS `t_artikel` (
  `art_id` int(11) NOT NULL,
  `art_judul` varchar(128) NOT NULL,
  `art_img` varchar(128) DEFAULT NULL,
  `art_konten` text NOT NULL,
  `art_tag` varchar(128) DEFAULT NULL,
  `art_datecreated` datetime NOT NULL,
  `art_updatedlog` datetime DEFAULT NULL,
  `art_userlog` int(1) DEFAULT NULL,
  `art_deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_artikel`
--

INSERT INTO `t_artikel` (`art_id`, `art_judul`, `art_img`, `art_konten`, `art_tag`, `art_datecreated`, `art_updatedlog`, `art_userlog`, `art_deleted`) VALUES
(1, 'Judul Artikel', NULL, 'Lorem ipsum dolor sit amet conse adipis elit Assumenda repud eum veniam optio modi sit explicabo nisi magnam quibusdam.sit amet conse adipis elit Assumenda repud eum veniam optio modi sit explicabo nisi magnam quibusdam.', NULL, '2019-12-08 00:00:00', NULL, 1, 0),
(6, 'Ini adalah Judul Post artikel pertama', NULL, '<p>ccvcv</p>', NULL, '2019-12-08 10:08:39', NULL, NULL, 0),
(7, 'Ini adalah Judul Post artikel pertama', 'img_c34e936.png', 'Some of the things in here might not look like JavaScript to you. Don''t panic. This is the future.\r\n\r\nFirst of all, ES2015 (also known as ES6) is a set of improvements to JavaScript that is now part of the official standard, but not yet supported by all browsers, so often it isn''t used yet in web development. React Native ships with ES2015 support, so you can use this stuff without worrying about compatibility. import, from, class, and extends in the example above are all ES2015 features. If you aren''t familiar with ES2015, you can probably pick it up by reading through sample code like this tutorial has. If you want, this page has a good overview of ES2015 features.\r\n\r\nThe other unusual thing in this code example is <View><Text>Hello world!</Text></View>. This is JSX - a syntax for embedding XML within JavaScript. Many frameworks use a specialized templating language which lets you embed code inside markup language. In React, this is reversed. JSX lets you write your markup language inside code. It looks like HTML on the web, except instead of web things like <div> or <span>, you use React components. In this case, <Text> is a built-in component that displays some text and View is like the <div> or <span>.', NULL, '2019-12-08 10:09:28', '2019-12-08 14:55:55', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_tag`
--

CREATE TABLE IF NOT EXISTS `t_tag` (
  `tag_id` int(11) NOT NULL,
  `tag_name` varchar(128) NOT NULL,
  `tag_datecreated` datetime NOT NULL,
  `tag_updatedlog` datetime NOT NULL,
  `tag_userlog` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `user_id` int(11) NOT NULL,
  `user_nama` varchar(128) NOT NULL,
  `user_email` varchar(128) NOT NULL,
  `user_password` varchar(256) NOT NULL,
  `user_image` varchar(128) NOT NULL,
  `user_notlp` varchar(20) NOT NULL,
  `user_alamat` varchar(128) NOT NULL,
  `user_jabatan` varchar(128) NOT NULL,
  `user_bidang_ilmu` varchar(128) DEFAULT NULL,
  `user_instansi` varchar(128) NOT NULL,
  `user_jenis_usaha` varchar(128) DEFAULT NULL,
  `user_type` enum('perorangan','korporasi') NOT NULL,
  `user_cv` varchar(128) NOT NULL,
  `user_role` enum('superadmin','admin','member') NOT NULL,
  `user_isactive` int(1) NOT NULL,
  `user_archived` int(1) NOT NULL DEFAULT '0',
  `user_date_created` datetime NOT NULL,
  `user_updated_log` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`user_id`, `user_nama`, `user_email`, `user_password`, `user_image`, `user_notlp`, `user_alamat`, `user_jabatan`, `user_bidang_ilmu`, `user_instansi`, `user_jenis_usaha`, `user_type`, `user_cv`, `user_role`, `user_isactive`, `user_archived`, `user_date_created`, `user_updated_log`) VALUES
(1, 'Super Admin', 'admin@ptpi.com', '$2y$10$XgVBX3HKQu6z38Vy2szVpeAq8MRbax2d.1G66PFlyIAhnqV5sjoXS', 'default.jpg', '', '', '', '', '', NULL, '', '', 'superadmin', 1, 0, '2019-11-11 07:54:50', '0000-00-00 00:00:00'),
(2, 'Agus Dwi Ismawan', 'learn@inc.id', '$2y$10$ygmy4d1xEi7rr50nzqnnf.iqkGWDRzWWFoF1NOElfcbHUDXzWFV22', 'default.jpg', '085641345581', 'Jepara', 'IT Engineer', 'Informatika', 'KIC', '', 'korporasi', 'file_59683c9.pdf', 'member', 1, 0, '2019-11-21 08:30:39', '2019-12-08 11:57:47'),
(13, 'Sutejo', 'agusdwiismawan@gmail.com', '$2y$10$PazCfSJGoiB6MYumgW.nOebG8DbzfGNYLAt/DC5wfwsex/JdAWzwu', 'img_30a7476.jpg', '08521200921', 'Jaksell', 'sds', 'sdd', 'sdd', '', 'perorangan', 'file_735f505.pdf', 'member', 0, 0, '2019-11-25 13:54:55', NULL),
(18, 'Fikri', 'fik@ri.id', '$2y$10$ujVR7Z8bg1.g7b1BSkhbWuK6ucOHUFUeKszeHqw1pLSeXxwXQp2G.', 'img_1bfb91c.png', '08912121212', 'fgt', 'sdsd', 'sdsd', 'sds', '', 'perorangan', 'file_5e5a9be.pdf', 'member', 0, 0, '2019-12-06 23:12:55', NULL),
(21, 'Achmad', 'ah@ma.id', '$2y$10$CFbifuVrwf6cijqJj6BkceGqz71mEOj9X9BrNChx8v9ZgB3hZWEQq', 'img_b32e3a8.png', '0856413776', 'Surabaya', 'Manager', 'IT', 'ELS', '', 'perorangan', 'file_bce7db0.pdf', 'member', 0, 0, '2019-12-07 08:26:47', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_artikel`
--
ALTER TABLE `t_artikel`
  ADD PRIMARY KEY (`art_id`);

--
-- Indexes for table `t_tag`
--
ALTER TABLE `t_tag`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_artikel`
--
ALTER TABLE `t_artikel`
  MODIFY `art_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `t_tag`
--
ALTER TABLE `t_tag`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
